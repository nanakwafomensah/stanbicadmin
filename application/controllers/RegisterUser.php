
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class RegisterUser extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
            if(empty($this->session->userdata("logged_in")))
            {
                redirect(base_url(),'refresh');
            }

	 	}

public function index()
	{

        $users['select_users']=$this->select_users();
       // var_dump($users['select_users'])
		$this->load->view('userRegistration',$users);

	}

// [useremail] [nvarchar](50) NULL,
// 	[usersfullname] [nvarchar](50) NULL,
// 	[phonenumber] [nvarchar](50) NULL,
// 	[role] [nvarchar](50) NULL,
// 	[sex] [nvarchar](10) NULL,[petrausers]

	public function add_user()
                  {            
                         
                        $data=array(
                            'useremail' => $this->input->post('useremail')  ,
                            'usersfullname' => $this->input->post('usersfullname')  ,
                            'phonenumber' => $this->input->post('phonenumber')  ,
                            'role' => $this->input->post('role')  ,
                            'sex' => $this->input->post('sex')  ,
                              'status' => "A"                           
                         );
                       var_dump($data); die;
                            $this->db->insert('petrausers', $data);
                            redirect('userUtilities');
      
                    }


			public function select_users()
            {
                $query= $this->db->query
                ("select *  from petrausers where status='A' ");
                $select_users= $query->result();
                return $select_users;
            }

        public function update__branch()
            {
                $id=$this->input->post("id");
                $data = array( 
                   
                    'branchName' => $this->input->post('branchName')
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("branch", $data);
                redirect('userUtilities');
            }

            public function delete__branch()
            {
                $id=$this->input->post("id");
                $data = array( 
                    'status' => "D"
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("branch", $data);
                redirect('userUtilities');
            }

             
}

