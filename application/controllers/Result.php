
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//This is the Controller for codeigniter crud using ajax application.
class Result extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }

    public function index()
    {
        $fromdate=$this->session->flashdata('fromdate');
        $todate=$this->session->flashdata('todate');


        ////////////////////TOTAL
        $s=$this->db->query("select sum(amount) as total from trans  where datetrans >='$fromdate' and datetrans <='$todate'");
        $d=$this->db->query("select count(*) as count  from trans  where datetrans >='$fromdate' and datetrans <='$todate'");
          if($s->num_rows()){
                    /*amount*/
                    $result = $s->row();
                    $amount=$result->total;
                    $records['total'] = $amount;
                    /* count*/
                    $resultc = $d->row();
                    $count=$resultc->count;
                    $records['noOfTransaction'] = $count;

          }else{
                    /*amount*/
                    $amount= '0.00';
                    $records['total'] = $amount;
                    /* count*/
                    $count=0;
                    $records['noOfTransaction'] = $count;
          }
        ////////////////PENDING
        $s=$this->db->query("select sum(amount) as total from trans WHERE status='U' and datetrans >='$fromdate' and datetrans <='$todate' ");
        $d=$this->db->query("select count(*) as count  from trans  where status='U' and datetrans >='$fromdate' and datetrans <='$todate'");
        if($s->num_rows()){
                    /*amount*/
                    $result = $s->row();
                    $amount=$result->total;
                    $records['pending'] = $amount;
                    /*count*/
                    $resultc = $d->row();
                    $count=$resultc->count;
                    $records['noOfTransactionU'] = $count;
        }else{
                    /*amount*/
                    $amount= '0.00';
                    $records['pending'] = $amount;
                    /* count*/
                    $count=0;
                    $records['noOfTransactionU'] = $count;
        }
        ////////////////processed
        $s=$this->db->query("select sum(amount) as total from trans WHERE status='P' and datetrans >='$fromdate' and datetrans <='$todate'  ");
        $d=$this->db->query("select count(*) as count  from trans  where status='P' and datetrans >='$fromdate' and datetrans <='$todate'");

        if($s->num_rows()){
            $result = $s->row();
            $amount=$result->total;
            $records['processed'] = $amount;
            /*count*/
            $resultc = $d->row();
            $count=$resultc->count;
            $records['noOfTransactionP'] = $count;
        }else{
            $amount= '0.00';
            $records['processed'] = $amount;
            /* count*/
            $count=0;
            $records['noOfTransactionP'] = $count;
        }

        $query = $this->db->query("select * from trans where datetrans >='$fromdate' and datetrans <='$todate' ORDER BY CONVERT(DATE, datetrans) DESC,CONVERT(TIME, timetrans)");
        $records['trans'] = $query->result();
        $records['fromdate'] = $fromdate;
        $records['todate'] = $todate;

        $this->load->view('queryTransactions_view',$records);

    }



}

