
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class USerUtilitesController extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
            if(empty($this->session->userdata("logged_in")))
            {
                redirect(base_url(),'refresh');
            }
	 	}


public function index()
	{
		if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $users['usersfullname'] = $session_data['usersfullname'];
           
            $users['useremail'] = $session_data['useremail'];
          //  $users['logintoken'] = $session_data['logintoken'];
            
            $users['role'] = $session_data['role'];


		 $users['select_users']=$this->select_users();
		 $users['select_branch']=$this->select_branch();
		$this->load->view('userUtilities',$users);

		 }
        else
        {
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

	}
                 public function add_user(){
                         $password=$this->input->post('useremail');
                         $pass=strtolower($password);
                        $hashpassword=hash('sha512',$pass);  
                        $data=array(
                            'useremail' => $this->input->post('useremail')  ,
                            'usersfullname' => $this->input->post('usersfullname')  ,
                            'phonenumber' => $this->input->post('phonenumber')  ,
                            'role' => $this->input->post('role')  ,
                            'sex' => $this->input->post('sex')  ,
                            'userbranch' => $this->input->post('userbranch')  ,
                            'status' => "A" ,
                            'logintoken' => $hashpassword ,
                         );
                       $this->db->insert('petrausers', $data);
                        redirect('userUtilities');
      
                    }

    public function change_password()
{
        $password=$this->input->post("oldpass");
        $new_pass=$this->input->post("newpass");
        $new_pass_confirm=$this->input->post("newpassconf");
        $useremail=$this->input->post("useremail");

             $logintoken=hash('sha512',$password);

       // var_dump($useremail); die;
         $result=$this->validate_old_password($logintoken,$useremail);
         //echo($result); die;
         //var_dump($result); die();
                         if($result=='1')
                           {
                                        if($new_pass==$new_pass_confirm)
                                    {

                                                        $data = array('logintoken' => hash('sha512', $new_pass) );
                                                        $this->db->set($data);
                                                        $this->db->where('useremail',$useremail);
                                                       $this->db->update("petrausers",$data);

                                        echo $result;
                                    }
                                    else
                                    {
                                            //return 2;
                                            $result=2;
                                            echo $result;
                                    }
                                  
                           }
                           else{ 

                                echo $result;
                            }
                
}
    public function validate_old_password($logintoken,$useremail)
        {
            $query=$this->db->query("select logintoken from petrausers where logintoken='$logintoken' 
                and useremail='$useremail' "); 
           
              
                   $result=$query->num_rows();

                if($result >=1)
                {
                      // var_dump('1'); die;
                        return 1;
                    
                }
                else
                {
                     //var_dump('0'); die;
                        return 0;
                   
                }
        }
    public function select_users()
            {
                $query= $this->db->query
                ("select petrausers.*,branch.id as branchid,BranchName,SuspenseAccountNumber  from petrausers left join
 branch  on petrausers.userbranch = branch.id ");
                $select_users= $query->result();
                return $select_users;
            }
    public function update__users()
            {
                $id=$this->input->post("id");
                $data=array(
                            'useremail' => $this->input->post('useremail')  ,
                            'usersfullname' => $this->input->post('usersfullname')  ,
                            'phonenumber' => $this->input->post('phonenumber')  ,
                            'role' => $this->input->post('Role')  ,
                            'sex' => $this->input->post('sex')  ,
                            'userbranch' => $this->input->post('userbranch')  ,
                              'status' => "A"                           
                         );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("petrausers", $data);
                redirect('userUtilities');
            }


     public function select_branch()
            {
                $query= $this->db->query
                ("select *  from branch where status='A' ");
                $select_branch= $query->result();
                return $select_branch;
            }
     public function delete__user()
            {
                $id=$this->input->post("id");
                $data = array( 
                    'status' => "D"
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("petrausers", $data);
                redirect('userUtilities');
            }
     public function enable__user()
            {
                $id=$this->input->post("id");
                $data = array( 
                    'status' => "A"
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("petrausers", $data);
                redirect('userUtilities');
            }


}

