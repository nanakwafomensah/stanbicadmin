
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class RegisterBranch extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
            if(empty($this->session->userdata("logged_in")))
            {
                redirect(base_url(),'refresh');
            }
	 	}

public function index()
	{
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $branch['usersfullname'] = $session_data['usersfullname'];
            $branch['useremail'] = $session_data['useremail'];
            $branch['logintoken'] = $session_data['logintoken'];
            $branch['role'] = $session_data['role'];
        $branch['select_branch']=$this->select_branch();
       // var_dump($branch['select_branch']); die;
		$this->load->view('branchRegister', $branch);
        }
        else
        {
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

    }
 
	



		public function select_branch()
            {
                $query= $this->db->query
                ("select *  from branch where status='A' ");
                $select_banks_and_codes= $query->result();
                return $select_banks_and_codes;
            }

        public function update__branch()
            {
                $id=$this->input->post("id");
                $data = array( 
                   
                    'branchName' => $this->input->post('branchName'),
                    'branchsortcode' => $this->input->post('branchsortcode'),
                    'suspenceaccountnumber' => $this->input->post('suspenceaccountnumber')
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("branch", $data);
                redirect('regbranch');
            }

            public function delete__branch()
            {
                $id=$this->input->post("id");
                $data = array( 
                    'status' => "D"
                    );
                //var_dump($data); die;
                $this->db->set($data);
                $this->db->where('id',$id);
                $this->db->update("branch", $data);
                redirect('regbranch');
            }

             public function add_branch()
                  {            
                         
                        $data=array(
                            'BranchName' => $this->input->post('BranchName')  ,
                            'BranchSortCode' => $this->input->post('BranchSortCode')  ,
                            'SuspenseAccountNumber' => $this->input->post('SuspenseAccountNumber')  ,
                            'status' => "A"
                         );

                            $this->db->insert('branch', $data);
                            redirect('regbranch');
      
                    }

}

