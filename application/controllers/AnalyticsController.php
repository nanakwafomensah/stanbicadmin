
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class AnalyticsController extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
            if(empty($this->session->userdata("logged_in")))
            {
                redirect(base_url(),'refresh');
            }
	 	}

public function index()
	{
		if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $users['usersfullname'] = $session_data['usersfullname'];
            $users['useremail'] = $session_data['useremail'];
//            $users['logintoken'] = $session_data['logintoken'];
            $users['role'] = $session_data['role'];

            ///////////////////////**********DASHBOARD*******************////////////////////////
            //EXECUTE STORED PROCEDURE
            $this->load->database();
            $result =$this->db->query("DECLARE	@return_value int,
		@holdstring varchar(max)

EXEC	@return_value = [dbo].[sptrans]
		@holdstring = @holdstring OUTPUT

SELECT	@holdstring as N'@holdstring'

SELECT	'Return Value' = @return_value

")->result_array();


            foreach($result as $s){
                $values= $s['@holdstring'];
            }
            $splitteddata=explode("|",$values);
           /// echo $values;
            
//
            $users['revenueNum']= $splitteddata[0];
            $users['branchNum']= floor($splitteddata[1]);

            $users['userNum']= floor($splitteddata[2]);
            $users['activeNum']= floor($splitteddata[3]);
            $users['nonactiveNum']= floor($splitteddata[4]);
            $users['maleNum']= floor($splitteddata[5]);
            $users['femaleNum']= floor($splitteddata[6]);



            $users['jan']= $splitteddata[7];
            $users['feb']= $splitteddata[8];
            $users['mar']= $splitteddata[9];
            $users['apr']= $splitteddata[10];
            $users['may']= $splitteddata[11];
            $users['jun']= $splitteddata[12];
            $users['jul']= $splitteddata[13];
            $users['aug']= $splitteddata[14];
            $users['sep']= $splitteddata[15];
            $users['oct']= $splitteddata[16];
            $users['nov']= $splitteddata[17];
            $users['dec']= $splitteddata[18];
//
//            $records['lists'] = $this->get_all_roles();
//            $this->load->view('omcs_view',$records);








            ///////////////////////**********DASHBOARD*******************////////////////////////

		$this->load->view('analytics',$users);
     }
        else
        {
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

    }
}

