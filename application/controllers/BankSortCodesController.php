
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class BankSortCodesController extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
			if(empty($this->session->userdata("logged_in")))
			{
				redirect(base_url(),'refresh');
			}
	 	}

public function index()
	{
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $bankscodes['usersfullname'] = $session_data['usersfullname'];
            $bankscodes['useremail'] = $session_data['useremail'];
            $bankscodes['logintoken'] = $session_data['logintoken'];
            $bankscodes['role'] = $session_data['role'];
            $bankscodes['select_banks_and_codes']=$this->select_banks_and_codes();
//var_dump($bankscodes); die;
		$this->load->view('banksandsortcodes',$bankscodes);

 }
        else
        {
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

    }

// CREATE TABLE [dbo].[banks](
// 	[id] [int] NULL,
// 	[sortcode] [nvarchar](50) NULL,
// 	[bankname] [nvarchar](50) NULL,
// 	[status] [nchar](10) NULL

	public function select_banks_and_codes()
			{
				$query= $this->db->query
				("select id'id',sortcode'sortcode',bankname'bankname'  from banks where status='A' ");
				$select_banks_and_codes= $query->result();

				return $select_banks_and_codes;
			}

		public function update__banks_and_codes()
        	{
        		$id=$this->input->post("id");
        		$data = array( 
        			'sortcode' => $this->input->post('sortcode'),
        			'bankname' => $this->input->post('bankname')
        			);
        		//var_dump($data); die;
        		$this->db->set($data);
        		$this->db->where('id',$id);
        		$this->db->update("banks", $data);
        	    redirect('BankSortCodes');
        	}

        	public function delete__banks_and_codes()
        	{
        		$id=$this->input->post("id");
        		$data = array( 
        			'status' => "D"
        			);
        		//var_dump($data); die;
        		$this->db->set($data);
        		$this->db->where('id',$id);
        		$this->db->update("banks", $data);
        	    redirect('BankSortCodes');
        	}

        	 public function add_banks_and_codes()
                  {            
                         
                        $data=array(
                            'bankname' => $this->input->post('bankname')  ,
                              'sortcode' => $this->input->post('sortcode'),
                              'status' => "A"                           
                         );
                       // var_dump($data); die;
                            $this->db->insert('banks', $data);
                            redirect('BankSortCodes');
      
                    }
}

