
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class TransactionController extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
			if(empty($this->session->userdata("logged_in")))
			{
				redirect(base_url(),'refresh');
			}
	 	}

    public function index()
	{

		
		$this->load->view('queryTransactions');

	}
	
	public function querydata(){

		$fromdate=$this->input->post('dateFrom');
		$todate=$this->input->post('dateTo');

		$this->session->set_flashdata('fromdate', $fromdate);
		$this->session->set_flashdata('todate', $todate);
		
//flash data
		redirect('mainview');
//
	}


}

