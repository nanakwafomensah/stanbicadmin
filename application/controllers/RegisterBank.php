
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Controller for codeigniter crud using ajax application.
class RegisterBank extends CI_Controller {
 
public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
			if(empty($this->session->userdata("logged_in")))
			{
				redirect(base_url(),'refresh');
			}
	 	}

public function index()
	{

		//$this->load->view('RegisterBank');
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$users['usersfullname'] = $session_data['usersfullname'];

			$users['useremail'] = $session_data['useremail'];
			//  $users['logintoken'] = $session_data['logintoken'];

			$users['role'] = $session_data['role'];


//			$users['select_users']=$this->select_users();
//			$users['select_branch']=$this->select_branch();
			$this->load->view('RegisterBank',$users);

		}
		else
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



		public function Profile()
			{
				$query= $this->db->query
				("select id'id',corporateProfileTextParagraph'corporateProfileTextParagraph'  from profile ");
				$Profile= $query->result();

				return $Profile;
			}

		public function update_CoreValues()
        	{
        		$id=$this->input->post("id");
        		$data = array( 'corporateCoreValueTextParagraph' => $this->input->post('corporateCoreValueTextParagraph')	);
        		//var_dump($data); die;
        		$this->db->set($data);
        		$this->db->where('id',$id);
        		$this->db->update("CoreValues", $data);
        	    redirect('Corporate');
        	}
        	 public function addprofileparagraph()
                  {            
                         
                        $data=array(
                            'corporateProfileTextParagraph' => $this->input->post('corporateProfileTextParagraph')                           
                         );
                            $this->db->insert('profile', $data);
                            redirect('corporate');
      
                    }

}

