<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
//        if(empty($this->session->userdata("logged_in")))
//        {
//            redirect(base_url(),'refresh');
//        }
    }

    function index()
    {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('useremail', 'useremail', 'trim|required');
        $this->form_validation->set_rules('logintoken', 'logintoken', 'trim|required|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            //Field validation failed.  User redirected to login page
           // $this->load->view('login');
            $this->load->view('login');
        }
        else
        {
            if($this->session->userdata('logged_in')) {
                $session_data = $this->session->userdata('logged_in');
                $loginVerification['role'] = $session_data['role'];
                if($loginVerification['role']=="Admin")
                {
                    redirect('dashboard', 'refresh');
                }
               else if($loginVerification['role']=="Branch Collections")
                {
                    redirect('dashboard', 'refresh');
                }
                else if($loginVerification['role']=="User Management")
                {
                    redirect('dashboard', 'refresh');
                }
                else{
                    //Go to private area
                    redirect('login', 'refresh');
                }


            }
        }

    }

    function check_database($logintoken)
    {
          //Field validation succeeded.  Validate against database
        $useremail = $this->input->post('useremail');
        $g=hash('sha512',$logintoken);
        $result = $this->user->login($useremail, $g);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array(
                    
                    'usersfullname'=>$row->usersfullname,
                    'useremail'=>$row->useremail,
                    //'logintoken'=>$row->logintoken,
                    'role'=>$row->role,
                    'BranchName'=>$row->BranchName,
                    'BranchSortCode'=>$row->BranchSortCode,
                    'SuspenseAccountNumber'=>$row->SuspenseAccountNumber

                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}
?>