<?php
Class User extends CI_Model
{
    function login($useremail, $logintoken)
    {


        // $query= $this->db->query("select user_email, password from users where user_email='$user_email' and password='$user_password' ");
        $status="A";
       // $delete_flag="N";

        $this -> db -> select('petrausers.id,petrausers.useremail,petrausers.logintoken, petrausers.usersfullname,petrausers.role,branch.id as branchid,BranchName,BranchSortCode,SuspenseAccountNumber');
        $this -> db -> from('petrausers');
        $this -> db->  join('branch', 'petrausers.userbranch = branch.id', 'left');
        $this -> db -> where('petrausers.useremail', $useremail);
        $this -> db -> where('petrausers.logintoken', $logintoken);
        $this -> db -> where('petrausers.status', $status);


        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
?>