<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="admin/dist/img/favicon.ico">
    <link rel="icon" href="admin/dist/img/favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="dashboard">
                        <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" height ="36px" width="156px"/>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <br>

                 <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
               <!--  <input type="hidden" name="email" id="my_email_address" value="<?php echo $useremail;?>"> -->
 |
    <a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>


        
        </div>
    </nav>
    
     <?php include ('templates/leftside.php') ;?>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->

            <!-- /Row -->

            <!-- Row -->
            <div class="row">
<form method="post" action="BankSortCodesController/add_banks_and_codes" >
                <div class="col-sm-2">
                    <label class="control-label mb-10 text-left" style="font-size: 11px;">Bank Name </label>
                    <input type="text" class="form-control" name="bankname" placeholder="john doe..." style="font-size: 11px;" required="">
                    </div>
                <div class="col-sm-2">
                    <label class="control-label mb-10 text-left" style="font-size: 11px;">Sort Code </label>
                    <input type="text" class="form-control" name="sortcode" placeholder="john doe..." style="font-size: 11px;" required="">
                </div>

                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success  pull-right btn-xs " style = " ; background-color: forestgreen; border-color: forestgreen; margin-top: 35px"><i class="fa fa-database"></i><span class="btn-text"> Add Bank</span></button>

                </div>
</form>
                </div>
<br><hr>

           


            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap" style = "font-size: 12px">
                                    <div class="">
                                        <table id="myTable1" class="table table-hover display  pb-30" style = "font-size: 12px">
                                            <thead style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>Bank Name</th>
                                                <th>Sort Code</th>
                                               
                                                 <th>Actions</th>

                                                
                                            </tr>
                                            </thead>
                                            <tfoot style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>Bank Name</th>
                                                <th>Sort Code</th>
                                                <!-- <th>Status</th> -->
                                                <th>Actions</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                                   <?php foreach($select_banks_and_codes as $record):?>
                                            <tr>
                                                
                                                <td> <?php echo $record->bankname;?></td>
                                                <td> <?php echo $record->sortcode;?></td>
                                                                                           
                                                <td>
                                                    <button type="submit" class="editBankSortCodes btn btn-warning  pull-middle btn-xs btn-circle" style = " " data-toggle="modal" data-target="#BankSortCodes" 
                                                    data-id="<?php echo $record->id;?>"
                                                     data-bankname="<?php echo $record->bankname;?>"
                                                        data-sortcode="<?php echo $record->sortcode;?>" 
                                                        ><i class="fa fa-edit"></i><span class="btn-text" ></span></button> 

                                                        
                                                     <button type="submit" class="deleteBankSortCodes btn btn-warning  pull-middle btn-xs btn-circle" style = " " data-toggle="modal" data-target="#BankSortCodesdelete" 
                                                    data-id="<?php echo $record->id;?>" 
                                                    data-bankname="<?php echo $record->bankname;?>"
                                                    ><i class="fa fa-trash"></i><span class="btn-text"></span></button> 
                                                      
                                                </td>
                                               <!--  <td><i class="fa fa-thumbs-up"></i> </td> -->
                                            </tr>
                                          
                                             <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

    <!-- Row -->

    <!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).on('click','.editBankSortCodes',function(){
            //SHOW MODAL

            $('#ideditBankSortCodes').val($(this).data('id'));
            $('#banknameeditBankSortCodes').val($(this).data('bankname'));
            $('#sortcodeeditBankSortCodes').val($(this).data('sortcode'));
        });
     $(document).on('click','.deleteBankSortCodes',function(){
            //SHOW MODAL
             $('#iddelete').val($(this).data('id'));
            var iddelete=$(this).data('bankname');
            $('#banknamedeletedg').html(iddelete);
        });
        $(document).on('click','#mychangepass,.enableuser',function(){
            //SHOW MODAL
             $('#mypasswordchangemodal').val($(this).data('useremail'));
            var iddelete=$(this).data('usersfullname');
            $('#deleteusername').html(iddelete);


            $('#enableuserid').val($(this).data('id'));
            var iddelete=$(this).data('usersfullname');
            $('#enableusername').html(iddelete);
        });
</script>

<!-- Data table JavaScript -->
<script src="admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="admin/dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="admin/dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="admin/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="admin/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="admin/dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="admin/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="admin/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="admin/dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<script src="admin/dist/js/chartjs-data.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="admin/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="admin/dist/js/init.js"></script>
<script src="admin/dist/js/dashboard-data.js"></script>
</body>

</html>
