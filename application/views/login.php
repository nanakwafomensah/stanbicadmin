<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="admin/dist/img/favicon.ico">
    <link rel="icon" href="admin/dist/img/favicon.ico" type="image/x-icon">

    <!-- vector map CSS -->
    <link href="admin/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>



    <!-- Custom CSS -->
    <link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0">
    <div height="15px" style="background-color: #26428b; color: #fff;">&nbsp</div>
</div>
    <header class="sp-header">
        <center>
        <div class="sp-logo-wrap center">
            <a href="dashboard">
                <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" width="311" height="76"/>
            </a>
        </div></center>

        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h6 class="text-center  mb-10" style = "color: #26428b; font-size: 12px">Backend Admin Portal</h6>


                                </div>
                                 <?php echo validation_errors(); ?>
                                  <?php echo form_open('Verifylogin'); ?>
                                <div class="form-wrap">
                                    <form action="#"  method="post" >
                                        <div class="form-group "  >
                                            <label class="control-label mb-8" for="exampleInputEmail_2" style = "font-size: 12px">Email address</label>
                                            <input type="email" name="useremail" class="form-control" required="" id="exampleInputEmail_2" placeholder="kofi@stanbic.com.gh" style = "font-size: 12px" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2" style = "font-size: 12px">Password</label>
                                            <a class="capitalize-font block mb-10 pull-right font-12" href="forgotpassword" style = "color: #26428b">forgot password ?</a>
                                            <div class="clearfix"></div>
                                            <input type="password" name="logintoken" class="form-control" required="" id="exampleInputpwd_2" placeholder="" style = "font-size: 12px" required="">
                                        </div>


                                        <div class="form-group text-center">
                                           <!--   <a href="logout"><i class="ti-power-off m-r-5"></i> Logout</a> -->
                                              <button type="submit"   class="btn btn-success" style = "font-size: 12px; background-color:#26428b ">Sign In</button>
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
        <div height="15px" style="background-color: #26428b; color: #fff;position: fixed;
    bottom: 0;
    width: 100%; background-size: cover;">&nbsp</div>
    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="admin/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="admin/dist/js/jquery.slimscroll.js"></script>

<!-- Init JavaScript -->
<script src="admin/dist/js/init.js"></script>
</body>
</html>
