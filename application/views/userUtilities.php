<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="dist\img\favicon.ico">
    <link rel="icon" href="admin/dist/img/favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="dashboard">
                        <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" height ="36px" width="156px"/>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <br>

                <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
               <!--  <input type="hidden" name="email" id="my_email_address" value="<?php echo $useremail;?>"> -->
 |
    <a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>

           <!--  <ul class="nav navbar-right top-nav pull-right">




                <li class="dropdown auth-drp">

                    <a href="login" class="dropdown-toggle pr-0" data-toggle="dropdown">  <span style = "font-size: 10px"> Welcome, Nana</span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>

                </li>
            </ul> -->
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->

     <?php include ('templates/leftside.php') ;?>
<!-- /Left Sidebar Menu -->

<!-- Right Sidebar Menu -->

<!-- /Right Sidebar Menu -->

<!-- Right Setting Menu -->


<!-- Right Sidebar Backdrop -->
<div class="right-sidebar-backdrop"></div>
<!-- /Right Sidebar Backdrop -->

<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid pt-25">

        <!-- Row -->

        <!-- /Row -->

        <!-- Row -->


        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark" style = "font-size: 12px">Users Information Pool</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap" style = "font-size: 12px">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display  pb-30" style = "font-size: 12px">
                                        <thead style = "font-size: 12px">
                                        <tr style = "font-size: 12px">
                                            <th>Email</th>
                                            <th>FullName</th>
                                            <th>Phone Number</th>
                                            <th>Gender</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot style = "font-size: 12px">
                                        <tr style = "font-size: 12px">
                                            <th>Email</th>
                                            <th>FullName</th>
                                            <th>Phone Number</th>
                                            <th>Gender</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                            <?php foreach($select_users as $record):?>
                                        <tr>
                                            <td> <?php echo $record->useremail;?></td>
                                             <td> <?php echo $record->usersfullname;?></td>
                                              <td> <?php echo $record->phonenumber;?></td>
                                               <td> <?php echo $record->sex;?></td>
                                                <td> <?php echo $record->role. ' ('."<strong>".$record->BranchName."</strong>".')';?></td>




                                            <td>
                                               <button type="submit" class="useredit btn btn-warning  pull-middle btn-xs btn-circle" style="font-weight: 900"  data-toggle="modal" data-target="#edituser"
                                                    data-id="<?php echo $record->id;?>"
                                                     data-useremail="<?php echo $record->useremail;?>"
                                                     data-usersfullname="<?php echo $record->usersfullname;?>"
                                                     data-phonenumber="<?php echo $record->phonenumber;?>"
                                                     data-sex="<?php echo $record->sex;?>"
                                                     data-role="<?php echo $record->role;?>"
                                                     data-branch="<?php echo $record->branchid;?>"

                                                ><i class="fa fa-edit"
                                                     ></i><span class="btn-text"></span></button> 
                                                <!-- <button type="submit" class="btn btn-danger  pull-middle btn-xs btn-circle" style = "font-weight: 900; background-color: maroon; border-color: maroon"><i class="fa fa-lock"></i><span class="btn-text"></span></button> -->
                                            
                                             <?php if(($record->role!='Admin')  and ($record->status=='A')){?>|
                                                 <button type="submit" class="deleteuser btn btn-warning  pull-middle btn-xs btn-circle" style = "font-weight: 900; background-color: red; border-color: red"  data-toggle="modal" data-target="#deleteusermodal" 
                                                     data-id="<?php echo $record->id;?>"
                                                     data-useremail="<?php echo $record->useremail;?>"
                                                     data-usersfullname="<?php echo $record->usersfullname;?>"
                                                     data-phonenumber="<?php echo $record->phonenumber;?>"
                                                     data-sex="<?php echo $record->sex;?>"
                                                     data-role="<?php echo $record->role;?>"><i class="fa fa-lock"
                                                     data-branch="<?php echo $record->branchid;?>"
                                                     ></i><span class="btn-text"></span></button>  

                                                <?php }?>
                                                <?php if(($record->role!='Admin')  and ($record->status=='D')){?>|
                                                     <button type="submit" class="enableuser btn btn-warning  pull-middle btn-xs btn-circle" style = "font-weight: 900; background-color: green; border-color: green"  data-toggle="modal" data-target="#enableusermodal" 
                                                        data-id="<?php echo $record->id;?>"
                                                         data-useremail="<?php echo $record->useremail;?>"
                                                         data-usersfullname="<?php echo $record->usersfullname;?>"
                                                         data-phonenumber="<?php echo $record->phonenumber;?>"
                                                         data-sex="<?php echo $record->sex;?>"
                                                         data-role="<?php echo $record->role;?>"
                                                             data-branch="<?php echo $record->branchid;?>"
                                                     ><i class="fa fa-unlock"
                                                         ></i><span class="btn-text"></span></button>

                                                   <?php }?>
                                                </td>
                                        </tr>
                                       
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- /Row -->

<!-- Row -->

<!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="admin/dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="admin/dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="admin/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="admin/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="admin/dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="admin/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="admin/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="admin/dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<script src="admin/dist/js/chartjs-data.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="admin/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="admin/dist/js/init.js"></script>
<script src="admin/dist/js/dashboard-data.js"></script>

<script type="text/javascript">
    $(document).on('click','.useredit,.magbin',function(){

            $('#edituserid').val($(this).data('id'));
                $('#edituserrole').val($(this).data('role')).change();
            $('#editusermail').val($(this).data('useremail'));
             $('#edituserfullname').val($(this).data('usersfullname'));
              $('#edituserphonenumber').val($(this).data('phonenumber'));
               $('#editusersex').val($(this).data('sex')).change();
               $('#editbranchx').val($(this).data('branch')).change();


        });
     $(document).on('click','.deleteuser,.enableuser',function(){
            //SHOW MODAL
             $('#deleteuserid').val($(this).data('id'));
            var iddelete=$(this).data('usersfullname');
            $('#deleteusername').html(iddelete);


            $('#enableuserid').val($(this).data('id'));
            var iddelete=$(this).data('usersfullname');
            $('#enableusername').html(iddelete);
        });
      $(document).on('click','#mychangepass,.enableuser',function(){
            //SHOW MODAL
             $('#mypasswordchangemodal').val($(this).data('useremail'));
            var iddelete=$(this).data('usersfullname');
            $('#deleteusername').html(iddelete);


            $('#enableuserid').val($(this).data('id'));
            var iddelete=$(this).data('usersfullname');
            $('#enableusername').html(iddelete);
        });
       
</script>




</body>

</html>
