<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="dist\img\favicon.ico">
    <link rel="icon" href="admin/dist/img/favicon.ico" type="image/x-icon">
	
	<!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
	
	<!-- Data table CSS -->

    <link href="admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


	<link href="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
	<!-- Custom CSS -->
	<link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
	<!-- Preloader -->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!-- /Preloader -->
    <div class="wrapper theme-1-active pimary-color-red">
		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="dashboard">
                            <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" height ="36px" width="156px"/>
						</a>
					</div>
				</div>	
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
				<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

			</div>
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">



<br>


   <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
               <!--  <input type="hidden" name="email" id="my_email_address" value="<?php echo $useremail;?>"> -->
 |
    <a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>

				</ul>
			</div>	
		</nav>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<!-- <div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span >Data Analytics</span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a class="active" href="dashboard" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

				</li>

				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Data Mangement</span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Blocks</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
						<li>
							<a href="regbank" style = "font-size: 11px">Register A Branch</a>
						</li>
						<li>
							<a href="reguser" style = "font-size: 11px">Register A User</a>
						</li>

                        <li>
                            <a href="#" style = "font-size: 11px">User Utilities</a>
                        </li>

                        <li>
                            <a href="bankRegister.html" style = "font-size: 11px">Banks & Sort Codes</a>
                        </li>

					</ul>
				</li>

				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Reports</span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="transaction" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions</span></div><div class="pull-right"></div><div class="clearfix"></div></a>


				</li>


			</ul>
		</div> -->
        <?php include ('templates/leftside.php') ;?>
		<!-- /Left Sidebar Menu -->
		
		<!-- Right Sidebar Menu -->

		<!-- /Right Sidebar Menu -->
		
		<!-- Right Setting Menu -->

		
		<!-- Right Sidebar Backdrop -->
		<div class="right-sidebar-backdrop"></div>
		<!-- /Right Sidebar Backdrop -->

        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				
				<!-- Row -->
				<div class="row">
<!--                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">-->
<!--                        <div class="panel panel-default card-view pa-0">-->
<!--                            <div class="panel-wrapper collapse in">-->
<!--                                <div class="panel-body pa-0">-->
<!--                                    <div class="sm-data-box bg-red">-->
<!--                                        <div class="container-fluid">-->
<!--                                            <div class="row">-->
<!--                                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">-->
<!--                                                    <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px">--><?php //echo $branchNum;?><!--</span></span>-->
<!--                                                    <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >Registered Branches</span>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">-->
<!--                                                    <i class="fa fa-bank txt-light data-right-rep-icon"></i>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">-->
<!--                        <div class="panel panel-default card-view pa-0">-->
<!--                            <div class="panel-wrapper collapse in">-->
<!--                                <div class="panel-body pa-0">-->
<!--                                    <div class="sm-data-box bg-red">-->
<!--                                        <div class="container-fluid">-->
<!--                                            <div class="row">-->
<!--                                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">-->
<!--                                                    <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px">--><?php //echo $userNum;?><!--</span></span>-->
<!--                                                    <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >Total Users</span>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">-->
<!--                                                    <i class="fa fa-users txt-light data-right-rep-icon"></i>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->



                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="sm-data-box" style = "background-color: gainsboro">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                    <span class="txt-dark block counter"><span class="counter-anim" style = "font-size: 20px"> <?php echo $revenueNum;?></span></span>
                                                    <span class="weight-500 uppercase-font txt-dark block font-13" style = "font-size: 18px"> Year To Date (GHS)</span>
                                                </div>
                                                <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-money txt-dark data-right-rep-icon"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

				</div>
				<!-- /Row -->
				
				<!-- Row -->
				<div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark" style = "font-size: 12px">User Status Distribution</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <canvas id="chart_7" height="80"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark" style = "font-size: 12px">User Gender Distribution</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <canvas id="chart_8" height="80"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">


                    <div class="col-lg-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark" style = "font-size: 12px">Transaction Statistics</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in" style = "font-size: 8px">
                                <div id="morris_extra_bar_chart" class="morris-chart"  style = "font-size: 8px"></div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
				</div>
				<!-- /Row -->
				
				<!-- Row -->

				<!-- Row -->
			</div>
			
			<!-- Footer -->

			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    

    <!-- Data table JavaScript -->
    <script src="admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="admin/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="admin/dist/js/responsive-datatable-data.js"></script>
	<!-- Slimscroll JavaScript -->
	<script src="admin/dist/js/jquery.slimscroll.js"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="admin/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="admin/dist/js/simpleweather-data.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="admin/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="admin/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="admin/dist/js/dropdown-bootstrap-extended.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
    <script >

            var jan= "<?php echo $jan;?>";
            var feb= "<?php echo $feb;?>";
            var mar= "<?php echo $mar;?>";
            var apr= "<?php echo $apr;?>";
            var may= "<?php echo $may;?>";
            var jun= "<?php echo $jun;?>";
            var jul= "<?php echo $jul;?>";
            var aug= "<?php echo $aug;?>";
            var sep= "<?php echo $sep;?>";
            var oct= "<?php echo $oct;?>";
            var nov= "<?php echo $nov;?>";
            var dec= "<?php echo $dec;?>";




            if($('#morris_extra_bar_chart').length > 0)
            // Morris bar chart
                Morris.Bar({
                    element: 'morris_extra_bar_chart',
                    data: [{
                        y: 'Jan',
                        TotalContributions: jan

                    }, {
                        y: 'Feb',
                        TotalContributions: feb

                    }, {
                        y: 'Mar',
                        TotalContributions: mar

                    }, {
                        y: 'Apr',
                        TotalContributions: apr

                    }, {
                        y: 'May',
                        TotalContributions: may
                    },{
                        y: 'Jun',
                        TotalContributions: jun
                    }, {
                        y: 'Jul',
                        TotalContributions: jul
                    }, {
                        y: 'Aug',
                        TotalContributions: aug
                    }, {
                        y: 'Sep',
                        TotalContributions: sep
                    }, {
                        y: 'Oct',
                        TotalContributions: oct
                    }, {
                        y: 'Nov',
                        TotalContributions: nov
                    }, {
                        y: 'Dec',
                        TotalContributions: dec
                    }],
                    xkey: 'y',
                    ykeys: ['TotalContributions'],
                    labels: ['TotalContributions'],
                    barColors:['#26428b'],
                    hideHover: 'auto',
                    gridLineColor: '#878787',
                    resize: true,
                    gridTextColor:'#878787',
                    gridTextFamily:"Roboto",
                    gridTextSize: '10'
                });

//        });

    </script>

    <!-- Sparkline JavaScript -->
	<script src="admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- ChartJS JavaScript -->
	<script src="admin/vendors/chart.js/Chart.min.js"></script>
    <!-- ChartJS JavaScript -->
    <script src="admin/vendors/chart.js/Chart.min.js"></script>
    <script >
       var x=<?php echo $activeNum;?>;
       var y=<?php echo $nonactiveNum;?>;

       var a=<?php echo $maleNum;?>;
       var b=<?php echo $femaleNum;?>;
        if( $('#chart_8').length > 0 ){
            var ctx7 = document.getElementById("chart_8").getContext("2d");
            var data7 = {
                labels: [

                    "Male",
                    "Female"
                ],
                datasets: [
                    {
                        data: [ a, b],
                        backgroundColor: [

                            "grey",
                            "maroon"
                        ],
                        hoverBackgroundColor: [

                            "grey",
                            "maroon"
                        ]
                    }]
            };

            var doughnutChart = new Chart(ctx7, {
                type: 'doughnut',
                data: data7,
                options: {
                    animation: {
                        duration:	3000
                    },
                    responsive: true,
                    legend: {
                        labels: {
                            fontFamily: "Roboto",
                            fontColor:"#878787"
                        }
                    },
                    tooltips: {
                        backgroundColor:'rgba(33,33,33,1)',
                        cornerRadius:0,
                        footerFontFamily:"'Roboto'"
                    },
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }
            });
        }


        if( $('#chart_7').length > 0 ){
            var ctx7 = document.getElementById("chart_7").getContext("2d");
            var data7 = {
                labels: [

                    "Active",
                    "NonActive"
                ],
                datasets: [
                    {
                        data: [ x, y],
                        backgroundColor: [

                            "grey",
                            "maroon"
                        ],
                        hoverBackgroundColor: [

                            "grey",
                            "maroon"
                        ]
                    }]
            };

            var doughnutChart = new Chart(ctx7, {
                type: 'doughnut',
                data: data7,
                options: {
                    animation: {
                        duration:	3000
                    },
                    responsive: true,
                    legend: {
                        labels: {
                            fontFamily: "Roboto",
                            fontColor:"#878787"
                        }
                    },
                    tooltips: {
                        backgroundColor:'rgba(33,33,33,1)',
                        cornerRadius:0,
                        footerFontFamily:"'Roboto'"
                    },
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }
            });
        }
    </script>
	<!-- Morris Charts JavaScript -->
    <script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
    <script src="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="admin/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="admin/dist/js/init.js"></script>
	<script src="admin/dist/js/dashboard-data.js"></script>
    <script type="text/javascript">
         $(document).on('click','#mychangepass,.enableuser',function(){
            //SHOW MODAL
             $('#mypasswordchangemodal').val($(this).data('useremail'));
            var iddelete=$(this).data('usersfullname');
            $('#deleteusername').html(iddelete);


            $('#enableuserid').val($(this).data('id'));
            var iddelete=$(this).data('usersfullname');
            $('#enableusername').html(iddelete);
        });
    </script>
</body>

</html>
