<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Xpozzit</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="admin/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
<!-- Top Menu Items -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="dashboard">
                    <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" height ="24px" width="76px"/>
                </a>
            </div>
        </div>
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
        <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
        <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <!-- <ul class="nav navbar-right top-nav pull-right">




            <li class="dropdown auth-drp"> -->
                <br>

                <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
               <!--  <input type="hidden" name="email" id="my_email_address" value="<?php echo $useremail;?>"> -->
 |
    <a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>



               <!--  <a href="login" >   <img src="admin/dist/img/ite.png" alt="user_auth" class="user-auth-img img-circle" style = "width: 40px; height: 30px"/> <span style = "font-size: 10px"> Welcome, Nana</span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a> -->

                <!--  <a href="login" >  <span style = "font-size: 10px"> Welcome, Nana</span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>|
                        <a href="#" ><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a> -->

           <!--  </li>
        </ul> -->
    </div>
</nav>
<!-- /Top Menu Items -->

<!-- Left Sidebar Menu -->
<!-- <div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span >Data Analytics</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Banks &amp Users</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Data Mangement</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="active"  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Client Sub-System</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="bankRegister.html" style = "font-size: 11px">Register A New Client</a>
                </li>
                <li>
                    <a href="modals.html" style = "font-size: 11px">Other Utilities</a>
                </li>

            </ul>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>System Audit</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Activity Logs</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

        </li>


    </ul>
</div> -->
    <?php include ('templates/leftside.php') ;?>
<!-- /Left Sidebar Menu -->

<!-- Right Sidebar Menu -->

<!-- /Right Sidebar Menu -->

<!-- Right Setting Menu -->


<!-- Right Sidebar Backdrop -->
<div class="right-sidebar-backdrop"></div>
<!-- /Right Sidebar Backdrop -->

<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- Title -->
<br>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark" style="font-size: 12px; font-weight: 500">Register A Client <i class = "fa fa-bank"></i></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-wrap">
                                        <form data-toggle="validator" role="form">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Client Name</label>
                                                <input type="text" class="form-control" id="inputName" placeholder="Information Technology Engineers Limited" required style = "font-size: 11px">
                                            </div>

                                                <div class="form-group col-md-6">
                                                    <label class="control-label mb-10" style = "font-size: 11px; font-weight: 500">Client Sector</label>
                                                    <select class="selectpicker" data-style="form-control " style = "font-size: 11px">
                                                        <option style = "font-size: 11px">Banks</option>
                                                        <option style = "font-size: 11px">NBFIs</option>
                                                        <option style = "font-size: 11px">MFIs</option>
                                                        <option style = "font-size: 11px">Insurance Firms</option>
                                                        <option style = "font-size: 11px">Hotels</option>
                                                    </select>
                                                </div></div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Head Office Location</label>
                                                    <input type="text" class="form-control" id="inputName" placeholder="2nd Floor, Damax Building, Chartfield Avenue, Adabrak-Accra" required style = "font-size: 11px">
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Web Address</label>
                                                    <input type="text" class="form-control" id="inputName" placeholder="www.ite.com.gh" style = "font-size: 11px">
                                                </div></div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Contact Person</label>
                                                    <input type="text" class="form-control" id="inputName" placeholder="Kweku Agyin" required style = "font-size: 11px">
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Contact Number</label>
                                                    <input type="text" class="form-control" id="inputName" placeholder="0233888999" style = "font-size: 11px">
                                                </div></div>


                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Admin Mail</label>
                                                    <input type="email" class="form-control" id="inputEmail" placeholder="kweku@ite.com.gh" data-error="The email address is invalid" required style = "font-size: 11px">
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label for="inputName" class="control-label mb-10" style="font-size: 12px; font-weight: 500">Corporate Logo</label>
                                                    <input type="file" name="pic" accept="image/*" style = "margin-top: 10px">
                                                </div>





                                            <div class="form-group col-md-3">

                                                <button type="submit" class="btn btn-success  pull-right btn-circle " style = "margin-top: 30px; font-weight: 900"><i class="fa fa-save"></i><span class="btn-text"></span></button>
                                            </div>   </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


    <!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /Row -->

<!-- Row -->

<!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="admin/dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="admin/dist/js/jquery.slimscroll.js"></script>
<script src="admin/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<!-- simpleWeather JavaScript -->
<script src="admin/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="admin/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="admin/dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="admin/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="admin/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="admin/dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<script src="admin/dist/js/chartjs-data.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="admin/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="admin/dist/js/init.js"></script>
<script src="admin/dist/js/dashboard-data.js"></script>
</body>

</html>
