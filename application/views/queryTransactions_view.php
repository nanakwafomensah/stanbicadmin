<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="admin/dist/img/favicon.ico">
    <link rel="icon" href="admin/dist/img/favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="admin/vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="admin/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="admin/dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="dashboard">
                        <img class="brand-img" src="admin/dist/img/logo.png" alt="brand" height ="36px" width="156px"/>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <br>

            <a href="login" >  <span style = "font-size: 10px"> Welcome, Nana</span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
            |
            <a href="#" data-toggle="modal" data-target="#changePassword" ><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>

            <!--  <ul class="nav navbar-right top-nav pull-right">
 
 
 
 
                 <li class="dropdown auth-drp">
 
                     <a href="login" class="dropdown-toggle pr-0" data-toggle="dropdown">  <span style = "font-size: 10px"> Welcome, Nana</span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
 
                 </li>
             </ul> -->
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <!--   <div class="fixed-sidebar-left">
          <ul class="nav navbar-nav side-nav nicescroll-bar">
              <li class="navigation-header">
                  <span >Data Analytics</span>
                  <i class="zmdi zmdi-more"></i>
              </li>
              <li>
                  <a  href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions &amp Clients</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
  
              </li>
  
              <li><hr class="light-grey-hr mb-10"/></li>
              <li class="navigation-header">
                  <span>Data Mangement</span>
                  <i class="zmdi zmdi-more"></i>
              </li>
              <li>
                  <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Users Mangement</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                  <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
                      <li>
                          <a href="bankRegister.html" style = "font-size: 11px">Register A Branch</a>
                      </li>
                      <li>
                          <a href="#" style = "font-size: 11px">Register A User</a>
                      </li>
  
                     
  
                  </ul>
              </li>
  
              <li><hr class="light-grey-hr mb-10"/></li>
              <li class="navigation-header">
                  <span>Reports</span>
                  <i class="zmdi zmdi-more"></i>
              </li>
              <li>
                  <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                  <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Audit trails</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
  
              </li>
  
  
          </ul>
      </div> -->
    <?php include ('templates/leftside.php') ;?>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">


            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-red">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px"><?php echo $total;?></span><?php echo '<p>['."No. Transactions:".$noOfTransaction.']<p>';?></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >(Total)</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box " style="background-color: #808080">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px"><?php echo $processed;?></span><?php echo '<p>['."No. Transactions:".$noOfTransactionP.']<p>';?></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >(Paid)</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-thumbs-up txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box " style="background-color: maroon">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px"><?php echo $pending;?></span><?php echo '<p>['."No. Transactions:".$noOfTransactionU.']<p>';?></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >(Unpaid)</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-thumbs-down txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>



            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap" style = "font-size: 12px">
                                    <div class="">
                                        <table id="myTable1" class="table table-hover display  pb-30" style = "font-size: 12px">
                                            <thead style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>Contributors Name</th>
                                                <th>Pension ID</th>
                                                <th>Value Date</th>

                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Payment By</th>
                                                <th>Payee's Contact</th>
                                                <th>Payment Mode</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tfoot style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>Contributors Name</th>
                                                <th>Pension ID</th>
                                                <th>Value Date</th>

                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Payment By</th>
                                                <th>Payee's Contact</th>
                                                <th>Payment Mode</th>
                                                <th>Status</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            <?php foreach ($trans as $record):?>

                                                <tr>
                                                    <td><?php echo strtoupper($record->contr_name); ?></td>
                                                    <td><?php echo strtoupper($record->pensionid); ?></td>
                                                    <td><?php echo strtoupper($record->valuedate); ?></td>
                                                    <td><?php echo strtoupper($record->amount); ?></td>
                                                    <td><?php echo strtoupper($record->datetrans); ?></td>

                                                    <td><?php echo strtoupper($record->timetrans); ?></td>
                                                    <td><?php echo strtoupper($record->payeesname); ?></td>
                                                    <td><?php echo strtoupper($record->payeescontact); ?></td>
                                                    <td><?php echo strtoupper($record->paymentmode ); ?></td>
                                                    <td><?php
                                                          if($record->status =='U'){
                                                              echo "UNPAID";
                                                          }elseif($record->status =='P'){
                                                               echo "PAID";
                                                          }
//
                                                        ?></td>





                                                </tr>

                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >


                <div class="col-sm-4">
                    <!--                    <label class="control-label mb-10 text-left" style="font-size: 11px;">From </label>-->
                    <input type="hidden" class="form-control" value="<?php echo  $fromdate;?>"  id="from_date" style="font-size: 11px;">
                </div>
                <div class="col-sm-4">
                    <!--                    <label class="control-label mb-10 text-left" style="font-size: 11px;">To</label>-->
                    <input type="hidden" class="form-control"  value="<?php echo  $todate;?>" id="to_date" style="font-size: 11px;">
                </div>


                <div class="col-sm-4">
                    <button id="btndownloadpdf" class="btn btn-success  pull-right btn-xs generatedata " style = " ; background-color: forestgreen; border-color: forestgreen; "><i class="fa fa-download"></i><span class="btn-text"> Download PDF </span></button> <div class="divider" style=" 

    display:inline-block;"/>

                    <button id="btndownloadexcel" class="btn btn-success  pull-right btn-xs generatedatax" style = " ; background-color: forestgreen; border-color: forestgreen;"><i class="fa fa-download"></i><span class="btn-text">  Download Excel </span></button>
                </div>

            </div>
            <br><hr>
        </div>
    </div>
    <!-- /Row -->

    <!-- Row -->

    <!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="admin/vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="admin/dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="admin/dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="admin/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="admin/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="admin/dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="admin/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="admin/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="admin/dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>
<script src="admin/dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="admin/vendors/chart.js/Chart.min.js"></script>
<script src="admin/dist/js/chartjs-data.js"></script>
<!-- Morris Charts JavaScript -->
<script src="admin/vendors/bower_components/raphael/raphael.min.js"></script>
<script src="admin/vendors/bower_components/morris.js/morris.min.js"></script>

<script src="admin/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="admin/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="admin/dist/js/init.js"></script>
<script src="admin/dist/js/dashboard-data.js"></script>
</body>
<script>
    $(document).on('click','.generatedata',function() {


            var fromdate = $("#from_date").val();
            var todate = $("#to_date").val();

            var  location = 'http://testvroofi.com/stanbicpdf_excel/pdf.php?type=1&date_from='+ fromdate +'&date_to='+ todate;
//        alert(location);
            window.location.href = location;

    });

    $(document).on('click','.generatedatax',function() {


            var fromdate = $("#from_date").val();
            var todate = $("#to_date").val();

            var  location = 'http://testvroofi.com/stanbicpdf_excel/excel.php?type=1&date_from='+ fromdate +'&date_to='+ todate;
//        alert(location);
            window.location.href = location;

    });
</script>
</html>
