		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span >Data Analytics</span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a class="active" href="dashboard" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

				</li>

				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Data Mangement</span>
					<i class="zmdi zmdi-more"></i>
			</li>
				<li>
<!--					<a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Blocks</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>-->
<!--					<ul id="ui_dr" class="collapse collapse-level-1 two-col-list">-->
						<li>
                          <a href="BankSortCodes" style = "font-size: 11px">Banks & Sort Codes</a>
                      </li>
						 <li>
							<a href="regbank" style = "font-size: 11px">Register A Bank</a>
						</li>
						<li>
							<a href="regbranch" style = "font-size: 11px">Register A Branch</a>
						</li>
						<li>
							<a href="#"  data-toggle="modal" data-target="#newuser" style = "font-size: 11px">Register A User</a>
						</li>

                        <li>
                            <a href="userUtilities" style = "font-size: 11px">User Utilities</a>
                        </li>



<!--					</ul>-->
				</li>

				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Reports</span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="transaction" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions</span></div><div class="pull-right"></div><div class="clearfix"></div></a>


				</li>


			</ul>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- /Left Sidebar Menu -->
		<!-- ///////////changing user password js//////////////////////////-->
    <script>
        $(document).on('click','#changepasssub',function(e) {
			e.preventDefault();
            var oldpass= $('#oldpass').val();
            var newpass = $('#newpass').val();
            var newpassconf = $('#newpassconf').val();
            var useremail=$('#mypasswordchangemodal').val();
           //alert(useremail);
            if(oldpass=="")
            {

            }
            else if(newpass=="")
            {

            }
            else if(newpassconf=="")
            {           }
            else{
                e.preventDefault();

                $.ajax({
                    type:'POST',
                    data:{oldpass:oldpass,newpass:newpass,newpassconf:newpassconf,useremail:useremail },
                    url:'<?php echo site_url('USerUtilitesController/change_password'); ?>',
                    success:function(result)
                    {
                    	//alert(result);
                        if(result==1)
                        {
                            $('#error_exist').html("Password Change successful");
                            location.reload();
                        }
                        else if(result==2) {
                            // window
                            $('#error_exist').html("Your New password Confirmation dont Match!!");
                            //location.reload();
                        }
                        else{
                            // window
                            $('#error_exist').html("Old Password is Wrong!!");
                            //location.reload();

                        }
                    }
                });
            }
        });
    </script>
		

                                      <!-- Modal -->
  <div class="modal fade" id="changePassword" role="dialog">
    <div class="modal-dialog">
    <!-- <form action="post" method="USerUtilitesController/change_password"> -->
<!-- <form> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title">Change Password</h4></center>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="field-1" class="control-label">Old Password</label>
                    <input type="text" id="oldpass" class="form-control" id="field-1" >
                     <input type="hidden" id="mypasswordchangemodal">
                </div>
            </div>
            
        </div>
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="field-1" class="control-label">New Password</label>
                    <input type="text" id="newpass" class="form-control" id="field-1"  >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="field-2" class="control-label">Confirm New Password</label>
                    <input type="text" id="newpassconf" class="form-control" id="field-2" >
                </div>
            </div>
        </div>
       
    </div>
        <div class="modal-footer">

        	<center>
        		<span id="error_exist" style="color:red; "></span><br>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" id="changepasssub" class="btn btn-info waves-effect waves-light"
         style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
          </center>
        </div>
      </div>
   <!--  </form> -->
    </div>
      
  </div>


   <div class="modal fade" id="BankSortCodes" role="dialog">
    <div class="modal-dialog">
    <form action="BankSortCodesController/update__banks_and_codes" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <center><h4 class="modal-title">Edit bank</h4></center>
        </div>

        <div class="modal-body">
      
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                	 <input type="hidden" id="ideditBankSortCodes" name="id" class="form-control" id="field-1" >
                    <label for="field-1" class="control-label">Bank Name</label>
                    <input type="text" id="banknameeditBankSortCodes" name="bankname" class="form-control" id="field-1" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="field-2" class="control-label">Sort Code</label>
                    <input type="text" id="sortcodeeditBankSortCodes"  name="sortcode" class="form-control" id="field-2" >
                </div>
            </div>
        </div>
       
    </div>
        <div class="modal-footer">
        	<center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </center>
        </div>
      </div>
      </form>
    </div>
  </div>


<div class="modal fade" id="BankSortCodesdelete" role="dialog">
    <div class="modal-dialog">
    <form action="BankSortCodesController/delete__banks_and_codes" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><p class="fa fa-trash" style="font-size:48px;color:red"></p></center>
       <!--    <h4 class="modal-title">delete Header</h4> -->
        </div>

        <div class="modal-body">
      
         <div class="row">
        
          <center><input type="hidden" name="id" id="iddelete">
         <p class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="banknamedeletedg" style="color: red"></span> ?</a></p></center>

            <!-- <center><span id="error_exist_new_user" style="color:red; "></span></center> -->
        </div>
       
    </div>
        <div class="modal-footer"><center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button></center>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>


<!----///BRANCH////-->
 <div class="modal fade" id="branchhedit" role="dialog">
    <div class="modal-dialog">
    <form action="RegisterBranch/update__branch" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title">Edit Branch</h4></center>
        </div>

        <div class="modal-body">
      
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                	 <input type="hidden" id="ideditbranch" name="id" class="form-control" id="field-1" >
                    <label for="field-1" class="control-label">Branch Name</label>
                    <input type="text" id="branchnameeditbranch" name="branchName" class="form-control" id="field-1" >
                </div>
            </div> 
             <div class="col-md-12">
                <div class="form-group">
                	 <input type="hidden" id="ideditbranch" name="id" class="form-control" id="field-1" >
                    <label for="field-1" class="control-label">Branch Sort Code</label>
                    <input type="text" id="branchsortcode" name="branchsortcode" class="form-control" id="field-1" >
                </div>
            </div> 
             <div class="col-md-12">
                <div class="form-group">
                	 <input type="hidden" id="ideditbranch" name="id" class="form-control" id="field-1" >
                    <label for="field-1" class="control-label">Suspense Account Number</label>
                    <input type="text" id="suspenseaccountnumber" name="suspenseaccountnumber" class="form-control" id="field-1" >
                </div>
            </div>
            
        </div>
       
    </div>
        <div class="modal-footer">
        	<center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </center>
        </div>
      </div>
      </form>
    </div>
  </div>


<div class="modal fade" id="branchdelete" role="dialog">
    <div class="modal-dialog">
    <form action="RegisterBranch/delete__branch" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><i class="fa fa-trash" style="font-size:48px;color:red"></i></center>
          <center><h4 class="modal-title">Delete Branch</h4></center>
        </div>

        <div class="modal-body">
      
         <div class="row">
        
          <center><input type="hidden" name="id" id="iddeletebranch">
         <p class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete <span id="branchnamedeletedgbranch" style="color: red"></span> ?</a></p></center>

            <!-- <center><span id="error_exist_new_user" style="color:red; "></span></center> -->
        </div>
       
    </div>
        <div class="modal-footer"><center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Delete Branch</button></center>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>



<!--/////////////////////////////////////////////////////////-->
<div class="modal fade" id="newuser" role="dialog">
    <div class="modal-dialog">
    <form action="USerUtilitesController/add_user" method="post">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <center> <h4 class="modal-title">New User</h4></center>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="field-1" class="control-label">Useremail</label>
                    <input type="text" name="useremail" class="form-control" id="field-1" required>
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group">
                    <label for="field-1" class="control-label">Sex</label>
                    <select name="sex"  class="form-control" required>
					  <option value="">Sex</option>
					  <option value="Male">Male</option>
					  <option value="Female">Female</option>
					</select>
                </div>
            </div>
                       
        </div>
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-1" class="control-label">Fullname</label>
                    <input type="text" name="usersfullname" class="form-control" id="field-1"required >
                </div>
            </div>
            
        </div>
         <div class="row">
         	 <div class="col-md-8">
                <div class="form-group">
                    <label for="field-2" class="control-label">Role</label>
                     <select name="role"  class="form-control" required>

					  <option value="">Role</option>
					  <option value="Admin">Admin</option>
					   <option value="Branch Collections">Branch Collections</option>
					  <option value="User Management">User Management</option>
					</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="field-1" class="control-label">Phonenumber</label>
                    <input type="text" name="phonenumber" class="form-control" id="field-1" required >
                </div>
            </div>
           
        </div>
         <div class="row">
         	 <div class="col-md-12">
                <div class="form-group">
                    <label for="field-2" class="control-label">User Branch</label>
                  
                     <select name="userbranch"  class="form-control" required>
                    <option value=""  >Select Branch</option>
				  <?php foreach($select_branch as $record):?>
					 <option value="<?php echo $record->id;?>"><?php echo $record->BranchName;?></option>
					 <?php endforeach;?>
					</select>
					   
                </div>
            </div>
           
           
        </div>
        
       
    </div>
        <div class="modal-footer"><center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important " >Add User</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --></center>
        </div>
      </div>
      </form>
    </div>
  </div>


  <div class="modal fade" id="edituser" role="dialog">
    <div class="modal-dialog">
    <form action="USerUtilitesController/update__users" method="post">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit User</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        	
            <div class="col-md-8">
            	<input type="hidden" id="edituserid" name="id" class="form-control"  >
                <div class="form-group">
                    <label for="field-1"  class="control-label">Useremail</label>
                    <input type="text" id="editusermail" name="useremail" class="form-control"  >
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group">
                    <label for="field-1" class="control-label">Sex</label>
                    <select name="sex"  id="editusersex"  class="form-control">
					  <option value="Male">Male</option>
					  <option value="Female">Female</option>
					</select>
                </div>
            </div>
                       
        </div>
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-1"  class="control-label">Fullname</label>
                    <input type="text" id="edituserfullname" name="usersfullname" class="form-control" >
                </div>
            </div>
            
        </div>
         <div class="row">
         	 <div class="col-md-8">
                <div class="form-group">
                    <label for="field-1" class="control-label">Role</label>
                    <select name="Role" id="edituserrole"   class="form-control">
                        <option value="">Role</option>
                        <option value="Admin">Admin</option>
                        <option value="Branch Collections">Branch Collections</option>
                        <option value="User Management">User Management</option>
					</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="field-1"  class="control-label">Phonenumber</label>
                    <input type="text" id="edituserphonenumber" name="phonenumber" class="form-control"  >
                </div>
            </div>
           
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-2" class="control-label">User Branch</label>

                        <select name="userbranch"  id="editbranchx" class="form-control" required>
                            <option value=""  >Select Branch</option>
                            <?php foreach($select_branch as $record):?>
                                <option value="<?php echo $record->id;?>"><?php echo $record->BranchName;?></option>
                            <?php endforeach;?>
                        </select>

                    </div>
                </div>


            </div>
       
    </div>
        <div class="modal-footer">
        	<center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </center>
        </div>
      </div>
      </form>
    </div>
  </div>

   <div class="modal fade" id="deleteusermodal" role="dialog">
    <div class="modal-dialog">
    <form action="USerUtilitesController/delete__user" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><i class="fa fa-trash" style="font-size:48px;color:red"></i></center>
         <center> <h4 class="modal-title">Lock user</h4></center>
        </div>

        <div class="modal-body">
      
         <div class="row">
        
          <center><input type="hidden" name="id" id="deleteuserid">
         <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Lock <span id="deleteusername" style="color: red"></span> ?</a></li></center> 

            <!-- <center><span id="error_exist_new_user" style="color:red; "></span></center> -->
        </div>
       
    </div>
        <div class="modal-footer"><center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Lock User</button></center>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>





  <div class="modal fade" id="enableusermodal" role="dialog">
    <div class="modal-dialog">
    <form action="USerUtilitesController/enable__user" method="post" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><i class="fa fa-unlock" style="font-size:48px;color:green"></i></center>
          <center><h4 class="modal-title">Enable user</h4></center>
        </div>

        <div class="modal-body">
      
         <div class="row">
        
          <center><input type="hidden" name="id" id="enableuserid">
         <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Unlock <span id="enableusername" style="color: red"></span> ?</a></li></center> 

            <!-- <center><span id="error_exist_new_user" style="color:red; "></span></center> -->
        </div>
       
    </div>
        <div class="modal-footer"><center>
        	 <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light" style="background-color: #26428b !important; border-color:#26428b !important ">Unlock User</button></center>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>

 