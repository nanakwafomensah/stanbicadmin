/*Chartjs Init*/

$( document ).ready(function() {
    "use strict";
	

    if( $('#chart_8').length > 0 ){
        var ctx7 = document.getElementById("chart_8").getContext("2d");
        var data7 = {
            labels: [

                "Male",
                "Female"
            ],
            datasets: [
                {
                    data: [ 50, 10],
                    backgroundColor: [

                        "grey",
                        "maroon"
                    ],
                    hoverBackgroundColor: [

                        "grey",
                        "maroon"
                    ]
                }]
        };

        var doughnutChart = new Chart(ctx7, {
            type: 'doughnut',
            data: data7,
            options: {
                animation: {
                    duration:	3000
                },
                responsive: true,
                legend: {
                    labels: {
                        fontFamily: "Roboto",
                        fontColor:"#878787"
                    }
                },
                tooltips: {
                    backgroundColor:'rgba(33,33,33,1)',
                    cornerRadius:0,
                    footerFontFamily:"'Roboto'"
                },
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });
    }


	if( $('#chart_7').length > 0 ){
		var ctx7 = document.getElementById("chart_7").getContext("2d");
		var data7 = {
			 labels: [

			"Active",
			"NonActive"
		],
		datasets: [
			{
				data: [ 50, 10],
				backgroundColor: [

					"grey",
					"maroon"
				],
				hoverBackgroundColor: [

                    "grey",
                    "maroon"
				]
			}]
		};
		
		var doughnutChart = new Chart(ctx7, {
			type: 'doughnut',
			data: data7,
			options: {
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					labels: {
					fontFamily: "Roboto",
					fontColor:"#878787"
					}
				},
				tooltips: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Roboto'"
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				}
			}
		});
	}	
});